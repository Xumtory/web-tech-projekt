let head = document.getElementsByTagName("head")[0];
let style = createLink();

document.getElementsByTagName("h1")[0].addEventListener("dblclick", () => {
    if (head.children.length > 4) {
        head.removeChild(style);
    } else {
        head.appendChild(style);
    }
});

function createLink() {
    let link = document.createElement("link");
    link.rel = "stylesheet";
    link.type = "text/css";
    link.href = "light-style.css";

    return link;
}
