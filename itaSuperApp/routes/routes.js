const express = require("express");
const database = require("../models/mockDatabase");
const router = express.Router();
const modul = require("../models/modul");
const abMiddleware = require("./aufgabeMiddleware");
const multer = require("multer");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'files/')
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
});
const upload = multer({ storage: storage });

//Raphael
var mediaList = database.mediaList;
var dokumente = mediaList.dokumente;
var bilder = mediaList.bilder;
var videos = mediaList.videos;
var podcasts = mediaList.podcasts;
router.get("/media/list", (req, res) => res.render("mediaList", { database, modul, req }));
router.get("/media/form", (req, res) => res.render("mediaForm", { database, modul }));
router.post("/media/searchResult", (req, res) => res.render("mediaSearchResult", { database, modul, req }));
router.get("/media/doc/:id", (req, res) => res.render("mediaDetail", { dokumente, req }));
router.get("/media/vid/:id", (req, res) => res.render("mediaDetail", { videos, req }));
router.get("/media/pic/:id", (req, res) => res.render("mediaDetail", { bilder, req }));
router.get("/media/pod/:id", (req, res) => res.render("mediaDetail", { podcasts, req }));
router.post("/media/listFilter", (req, res) => res.render("mediaList", { database, modul, req }));
router.post("/media/createMediaData", upload.single("myfile"), (req, res) => database.addData(req, res));
router.post('/media/download', (req, res) => {
    res.download(req.body.path);
  });

//Boris
router.get("/aufgabe/list", (req, res) => abMiddleware.filterArbeitsblatt(req, res));
router.get("/aufgabe/form", (req, res) => res.render("aufgabeForm", { modul }));
router.get("/aufgabe/:id", (req, res) => abMiddleware.returnArbeitsblatt(req, res));
router.post("/aufgabe/createArbeitsblatt", (req, res) => abMiddleware.createArbeitsblatt(req, res));
router.post("/aufgabe/delete", (req, res) => abMiddleware.deleteArbeitsblatt(req, res));

//Global
router.get("/404", (req, res) => res.status(404).render("404"));
router.get("/", (req, res) => res.render("start"));
router.use((req, res) => res.redirect("/404"))


module.exports = router;