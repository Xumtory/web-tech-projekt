const Arbeitsblatt = require("../models/arbeitsblatt");
const Aufgabe = require("../models/aufgabe");
const abDatabase = require("../models/abDatabase");
const modul = require("../models/modul");

function createArbeitsblatt(req, res) {
    if (req.body.typ !== undefined) {
        let ab = new Arbeitsblatt(req.body.titel, findModul(req.body.modul), req.body.autor);
        if (!Array.isArray(req.body.typ)) {
            ab.addAufgabe(createAufgabe(req.body.typ, req.body.aufgabe));
        } else {
            for (i = 0; i < req.body.typ.length; i++) {
                let pos = i * 5;
                ab.addAufgabe(createAufgabe(req.body.typ[i], req.body.aufgabe.slice(pos, pos + 5)));
            }
        }
        abDatabase.insertIntoHolder(ab);
    }
    res.redirect("/aufgabe/form");
}


function filterArbeitsblatt(req, res) {

    let t = req.query.t;
    let a = req.query.a;
    let m = req.query.m;

    let result = abDatabase.abHolder;

    if (t != undefined && t !== "") {
        result = result.filter((e) => {
            return e.arbeitsblatt.titel.toLowerCase().includes(t.toLowerCase());
        });
    }

    if (a != undefined && a !== "") {
        result = result.filter((e) => {
            if (e.arbeitsblatt.autor != undefined) {
                return e.arbeitsblatt.autor.toLowerCase().includes(a.toLowerCase());
            } else {
                return false;
            }
        });
    }

    if (m != undefined && m !== "none") {
        result = result.filter((e) => {
            return e.arbeitsblatt.modul.short.toLowerCase().includes(m.toLowerCase());
        });
    }

    res.render("aufgabeList", { modul, result });
}

function returnArbeitsblatt(req, res) {
    for (i = 0; i < abDatabase.abHolder.length; i++) {
        if (abDatabase.abHolder[i].id == req.params.id) {
            res.render("aufgabeDetail", { arbeitsblatt: abDatabase.abHolder[i].arbeitsblatt, aufgabentyp: Aufgabe.aufgabentyp, id: abDatabase.abHolder[i].id });
            return;
        }
    }
    res.redirect("/404");
}

function deleteArbeitsblatt(req, res) {
    for (i = 0; i < abDatabase.abHolder.length; i++) {
        if (req.body.id == abDatabase.abHolder[i].id) {
            abDatabase.abHolder.splice(i, 1);
            break;
        }
    }
    res.redirect("list");
}

//Helper function
function findModul(mod) {
    return modul[mod];
}

function createAufgabe(typ, aufgaben) {
    if (typ == Aufgabe.aufgabentyp.TEXT) {
        return new Aufgabe.Aufgabe(typ, aufgaben[0]);
    } else {
        return new Aufgabe.Aufgabe(typ, aufgaben[0], aufgaben.slice(1, 5));
    };
}

module.exports = { createArbeitsblatt, filterArbeitsblatt, returnArbeitsblatt, deleteArbeitsblatt };