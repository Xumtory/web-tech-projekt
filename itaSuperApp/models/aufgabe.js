class Aufgabe {

    constructor(typ, titel, auswahl) {
        this.typ = typ;
        this.titel = titel;
        this.auswahl = auswahl;
    }
}

const aufgabentyp = {
    TEXT: "text",
    CHECKBOX: "check",
    RADIOBUTTON: "radio"
};

module.exports = { Aufgabe, aufgabentyp };