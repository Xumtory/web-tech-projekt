class Podcast {
    constructor(path, name, author, dauer, kommentar, uploaddatum, modul) {
        this.path = path;
        this.name = name;
        this.author = author;
        this.dauer = dauer;
        this.kommentar = kommentar;
        this.uploaddatum = uploaddatum;
        this.modul = modul;
        this._id = Podcast.incrementId()
    }
    static incrementId() {
        if (!this.latestId) {
            this.latestId = 1;
        }
        else {
            this.latestId++;
        }
        return this.latestId;
    }
}

module.exports = Podcast;