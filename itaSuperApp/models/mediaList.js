
class MediaList {
    bilder = new Array();
    videos = new Array();
    podcasts = new Array();
    dokumente = new Array();

    addBild(bild) {
        this.bilder.push(bild);
    }

    addVideo(video) {
        this.videos.push(video);
    }

    addPodcast(podcast) {
        this.podcasts.push(podcast);
    }

    addDokument(dokument) {
        this.dokumente.push(dokument);
    }

    /*
        removeBild(pos) {
            this.bilder.splice(pos, 1);    
        }
        removeVideo(pos) {
            this.videos.splice(pos, 1);     
        }
        removePodcast(pos) {
            this.podcasts.splice(pos, 1);       
        }
        
        removeDokument(pos) {
            this.dokumente.splice(pos, 1);
        }
        */
}




module.exports = MediaList;