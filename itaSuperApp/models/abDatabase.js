const Arbeitsblatt = require("./arbeitsblatt");
const Aufgabe = require("./aufgabe");
const modul = require("./modul");

let abHolder = new Array();
let idCounter = 0;

//Init Data
let abA = new Arbeitsblatt("Arbeitsblatt für Web-Technologien", modul.webtech, "Dr. Karl Ess");
abA.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.TEXT, "Erklären Sie kurz den Begriff DOM."));
abA.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.CHECKBOX, "Welche HTTP-Methode existieren?", new Array("GET", "REMOVE", "POST", "UPGRADE")));
abA.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.TEXT, "Was ist das ISO/OSI-Modell?"));
abA.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.RADIOBUTTON, "Mit welchem Tag kann ein Formular in HTML erstellt werden?", new Array("body", "form", "table", "section")));
abA.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.TEXT, "Was versteht man unter SPA (Single-Page Application)"));

let abB = new Arbeitsblatt("Arbeitsblatt für Mensch-Computer-Interaktion", modul.mci);
abB.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.TEXT, "Was sind die 8 goldenen Regeln von Shneiderman"));
abB.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.TEXT, "Welche Regeln gibt es."));
abB.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.RADIOBUTTON, "Was versteht man unter Dark Pattern", new Array("Darkmode", "Schlechtes Design", "Böswilliges Design", "Schlecht einsehbares Design")));

let abC = new Arbeitsblatt("Arbeitsblatt für Programmierkurs 1", modul.pk1, "Bach");
abC.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.TEXT, "Erklären sie kurz den Begriff Race Condition."));
abC.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.TEXT, "Was versteht man unter der Main Methode?"));
abC.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.CHECKBOX, "Wofür werden Properties in JavaFX verwendet?", new Array("Überwachen des Status von Objekten", "Zuweisen von Attributen ", " Zum ändern von Eigenschaften", "Zum hinzufügen von Listenern")));
abC.addAufgabe(new Aufgabe.Aufgabe(Aufgabe.aufgabentyp.TEXT, "Was ist mit Polymorphie gemeint?"));

insertIntoHolder(abA);
insertIntoHolder(abB);
insertIntoHolder(abC);


//Helper function
function insertIntoHolder(arbeitsblatt) {
    abHolder.push({ id: idCounter++, arbeitsblatt: arbeitsblatt });
}


module.exports = { abHolder, insertIntoHolder };