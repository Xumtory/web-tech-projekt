class Modul {
    constructor(name, short) {
        this.name = name;
        this.short = short;
    }
}

let modulListe = {
    webtech: new Modul("Web-Technologien", "webtech"),
    swt1: new Modul("Softwaretechnik 1", "swt1"),
    pk1: new Modul("Programmierkurs 1", "pk1"),
    mci: new Modul("Mensch-Computer-Interaktion", "mci")
};

module.exports = modulListe;