class Arbeitsblatt {
    constructor(titel, modul, autor) {
        this.titel = titel;
        this.modul = modul;
        this.autor = autor;
    }

    aufgaben = new Array();

    addAufgabe(aufgabe) {
        this.aufgaben.push(aufgabe);
    }
}

module.exports = Arbeitsblatt;