class Bild {
    constructor(path, name, author, kommentar, uploaddatum, modul) {
        this.path = path;
        this.name = name;
        this.author = author;
        this.kommentar = kommentar;
        this.uploaddatum = uploaddatum;
        this.modul = modul;
        this._id = Bild.incrementId()
    }

    static incrementId() {
        if (!this.latestId) {
            this.latestId = 1;
        }
        else {
            this.latestId++;
        }
        return this.latestId;
    }
}

module.exports = Bild;