const modul = require("./modul");
const podcast = require("./podcast");
const bild = require("./bild");
const video = require("./video");
const dokument = require("./dokument");
const medList = require("./mediaList");




let mediaList = new medList();

/** Dummy Data zum testen */
b1 = new bild("files/sample/Landschaft.jpg","Landschaft.jpg", "Dr. Schweden", "Eine tolle Landschaft", new Date().toDateString(), modul.swt1);
b2 = new bild("files/sample/Berg.jpg","Berg.jpg", "Dr. Frankreich", "Ein toller Berg", new Date().toDateString(), modul.mci);
b3 = new bild("files/sample/Landschaft2.jpg","Landschaft2.jpg", "Dr. Schweden", "Eine tolle Landschaft", new Date().toDateString(), modul.swt1);
b4 = new bild("files/sample/Berg2.jpg","Berg2.jpg", "Dr. Frankreich", "Ein toller Berg", new Date().toDateString(), modul.mci);
p1 = new podcast("files/sample/AudioVL 1.mp3","AudioVL 1.mp3", "Dr. Jens", 420, "Thema: Pflanzen", new Date().toLocaleDateString(), modul.mci);
p2 = new podcast("files/sample/AudioVL 2.mp3","AudioVL 2.mp3", "Dr. Peng", 69, "Thema: Peng", new Date().toLocaleDateString(), modul.webtech);
p3 = new podcast("files/sample/AudioVL 3.mp3","AudioVL 3.mp3", "Dr. Jens", 420, "Thema: Pflanzen", new Date().toLocaleDateString(), modul.swt1);
p4 = new podcast("files/sample/AudioVL 4.mp3","AudioVL 4.mp3", "Dr. Peng", 69, "Thema: Peng", new Date().toLocaleDateString(), modul.pk1);
v1 = new video("files/sample/Video VL1.mp4","Video VL1.mp4", "Dr. Mega", 560, "Bitte schauen!", new Date().toDateString(), modul.pk1);
v2 = new video("files/sample/Video VL2.mp4","Video VL2.mp4", "Dr. Mini", 5600, "Bitte nicht schauen!", new Date().toDateString(), modul.swt1);
v3 = new video("files/sample/Video VL3.mp4","Video VL3.mp4", "Dr. Mega", 560, "Bitte schauen!", new Date().toDateString(), modul.pk1);
v4 = new video("files/sample/Video VL4.mp4","Video VL4.mp4", "Dr. Mini", 5600, "Bitte nicht schauen!", new Date().toDateString(), modul.pk1);
d1 = new dokument("files/sample/Formelsammlung.txt","Formelsammlung.txt", "Dr. Doof", "Tolle Formelsammlung", new Date().toDateString(), modul.webtech);
d2 = new dokument("files/sample/Cheat Sheet.txt","Cheat Sheet.txt", "Dr. Toll", "CHEATER!", new Date().toDateString(), modul.mci);
d3 = new dokument("files/sample/Formelsammlung 2.txt","Formelsammlung 2.txt", "Dr. Doof", "Tolle Formelsammlung", new Date().toDateString(), modul.swt1);
d4 = new dokument("files/sample/Cheat Sheet 2.txt","Cheat Sheet 2.txt", "Dr. Toll", "CHEATER!", new Date().toDateString(), modul.pk1);
mediaList.addBild(b1);
mediaList.addBild(b2);
mediaList.addBild(b3);
mediaList.addBild(b4);
mediaList.addDokument(d1);
mediaList.addDokument(d2);
mediaList.addDokument(d3);
mediaList.addDokument(d4);
mediaList.addPodcast(p1);
mediaList.addPodcast(p2);
mediaList.addPodcast(p3);
mediaList.addPodcast(p4);
mediaList.addVideo(v1);
mediaList.addVideo(v2);
mediaList.addVideo(v3);
mediaList.addVideo(v4);


function addData(request, response) {
    let module = request.body.module;
    let filter = request.body.filter;
    let file = request.file.filename;
    let kommentar = request.body.kommentar;
    let author = request.body.author;
    let dauer = request.body.dauer;
    let path = "files/" + file;

    if (filter === "docs") {
        switch (module) {
            case "webtech":
                mediaList.addDokument(new dokument(path, file, author, kommentar, new Date().toDateString(), modul.webtech));
                break;
            case "swt1":
                mediaList.addDokument(new dokument(path, file, author, kommentar, new Date().toDateString(), modul.swt1));
                break;
            case "mci":
                mediaList.addDokument(new dokument(path, file, author, kommentar, new Date().toDateString(), modul.mci));
                break;
            case "pk1":
                mediaList.addDokument(new dokument(file, author, kommentar, new Date().toDateString(), modul.pk1));
                break;
        }
    }
    else if (filter === "vids") {
        switch (module) {
            case "webtech":
                mediaList.addVideo(new video(path, file, author, dauer, kommentar, new Date().toDateString(), modul.webtech));
                break;
            case "swt1":
                mediaList.addVideo(new video(path, file, author, dauer, kommentar, new Date().toDateString(), modul.swt1));
                break;
            case "mci":
                mediaList.addVideo(new video(path, file, author, dauer, kommentar, new Date().toDateString(), modul.mci));
                break;
            case "pk1":
                mediaList.addVideo(new video(path, file, author, dauer, kommentar, new Date().toDateString(), modul.pk1));
                break;
        }
    }
    else if (filter === "pods") {
        switch (module) {
            case "webtech":
                mediaList.addPodcast(new podcast(path, file, author, dauer, kommentar, new Date().toDateString(), modul.webtech));
                break;
            case "swt1":
                mediaList.addPodcast(new podcast(path, file, author, dauer, kommentar, new Date().toDateString(), modul.swt1));
                break;
            case "mci":
                mediaList.addPodcast(new podcast(path, file, author, dauer, kommentar, new Date().toDateString(), modul.mci));
                break;
            case "pk1":
                mediaList.addPodcast(new podcast(path, file, author, dauer, kommentar, new Date().toDateString(), modul.pk1));
                break;
        }
    }
    else if (filter === "pics") {
        switch (module) {
            case "webtech":
                mediaList.addBild(new bild(path, file, author, kommentar, new Date().toDateString(), modul.webtech));
                break;
            case "swt1":
                mediaList.addBild(new bild(path, file, author, kommentar, new Date().toDateString(), modul.swt1));
                break;
            case "mci":
                mediaList.addBild(new bild(path, file, author, kommentar, new Date().toDateString(), modul.mci));
                break;
            case "pk1":
                mediaList.addBild(new bild(path, file, author, kommentar, new Date().toDateString(), modul.pk1));
                break;
        }
    }
    response.redirect("/media/form");
}

module.exports = {
    mediaList: mediaList,
    addData: addData
}
