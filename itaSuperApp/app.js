const express = require("express");
const app = express();
const routes = require("./routes/routes");
const dokument = require("./models/dokument");
const PORT = 8000;


app.set("view engine", "ejs");
app.set("views", "./views");
app.use(express.urlencoded({ extended: true }));
app.use(express.static("public"));
app.use(routes);
app.listen(PORT, () => console.log(`Server is listening on port ${PORT}...`));
