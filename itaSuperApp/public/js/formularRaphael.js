window.onload = function () {
    var myfile = document.getElementById("myfile");
    var radios = document.getElementsByName("filter");
    var dauer = document.getElementById("dauer");
    for (radio in radios) {
        radios[radio].onclick = function () {
            myfile.removeAttribute("disabled");
            if (this.value === "pods" || this.value === "vids") {
                dauer.removeAttribute("disabled");
            }
            else {
                dauer.setAttribute("disabled", "true");
            }
            if (this.value === "pods") {
                myfile.setAttribute("accept", "audio/*");
            }
            if (this.value === "vids") {              
                myfile.setAttribute("accept", "video/*");
            }
            if (this.value === "pics") {
                myfile.setAttribute("accept", "image/*");
            }
            if (this.value === "docs") {
                myfile.setAttribute("accept", ".odt,.doc,.docx,.pdf");
            }
        }
    }


};

