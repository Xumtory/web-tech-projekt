window.onload = function () {
    let docView = document.getElementById("docs");
    let vidView = document.getElementById("vids");
    let picView = document.getElementById("pics");
    let podView = document.getElementById("pods");

    var checkboxes = document.querySelectorAll('input[type=checkbox][name=filter]');
    checkboxes[0].addEventListener("change", function (event) {
        if (event.target.checked) {
            docView.style.display = "grid";
        } else {
            docView.style.display = "none";
        }
    });
    checkboxes[1].addEventListener("change", function (event) {
        if (event.target.checked) {
            vidView.style.display = "grid";
        } else {
            vidView.style.display = "none";

        }
    });
    checkboxes[2].addEventListener("change", function (event) {
        if (event.target.checked) {
            picView.style.display = "grid";
        } else {
            picView.style.display = "none";

        }
    });
    checkboxes[3].addEventListener("change", function (event) {
        if (event.target.checked) {
            podView.style.display = "grid";
        } else {
            podView.style.display = "none";
        }

    });

   


};
