
let addButton = document.getElementById("add-aufgabe");
let aufgaben = document.getElementById("aufgaben");

document.getElementsByTagName("body")[0].onload = addAufgabe;

let i = 0;

addButton.addEventListener("click", addAufgabe);

function addAufgabe() {
    let sec = document.createElement("section");
    sec.classList.add("aufgabe");

    sec.appendChild(createTextArea());
    let radCheck = createRadioCheckArea();
    sec.appendChild(radCheck);
    sec.appendChild(createSettingsArea(sec, radCheck));
    aufgaben.appendChild(sec);
}

function createSettingsArea(parent, radCheck) {

    let sec = document.createElement("section");
    sec.classList.add("aufgabenSettings");

    let select = document.createElement("select");
    select.name = "typ";

    let a = document.createElement("option");
    a.value = "text";
    a.appendChild(document.createTextNode("Text"));
    let b = document.createElement("option");
    b.value = "check";
    b.appendChild(document.createTextNode("Checkbox"));
    let c = document.createElement("option");
    c.value = "radio";
    c.appendChild(document.createTextNode("Radiobutton"));

    select.appendChild(a);
    select.appendChild(b);
    select.appendChild(c);

    select.addEventListener("change", (e) => {
        let contains = radCheck.classList.contains("hide");
        if (select.value == "text") {
            if (!contains) {
                radCheck.classList.add("hide");
            }
        } else {
            if (contains) {
                radCheck.classList.remove("hide");
            }
        }
    });

    let button = document.createElement("button");
    button.type = "button"
    button.classList.add("deleteButton");
    button.appendChild(document.createTextNode("Löschen"));

    button.addEventListener("click", () => parent.remove());

    sec.appendChild(select);
    sec.appendChild(button);

    return sec;
}

function createTextArea() {
    let label = document.createElement("label");
    label.classList.add("textareaLabel");
    label.appendChild(document.createTextNode("Aufgabe:"));

    let textarea = document.createElement("textarea");
    textarea.rows = 5;
    textarea.name = "aufgabe";
    textarea.required = true;
    textarea.placeholder = "Aufgabenstellung";
    label.appendChild(textarea);

    return label;
}

function createRadioCheckArea() {
    let sec = document.createElement("section");
    sec.classList.add("radioCheckArea");
    sec.classList.add("hide");

    for (i = 1; i <= 4; i++) {
        let input = document.createElement("input");
        input.type = "text";
        input.placeholder = i;
        input.name = "aufgabe";
        sec.appendChild(input);
    }

    return sec;
}
