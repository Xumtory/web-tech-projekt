let a = document.getElementsByTagName("a");
let pathname = location.pathname;
let map = new Map();

for (i = 0; i < a.length; i++) {
    map.set(a[i].pathname, i);
}
if (map.has(pathname)) {
    a[map.get(pathname)].id = "active";
    console.log("a");

    //Wird benötigt, um Raphaels Filter in der Navbar richtig aufzulösen
} else if (pathname.includes("/media/listFilter") || pathname.includes("/media/searchResult")) {
    if (map.has("/media/list")) {
        a[map.get("/media/list")].id = "active";
    }
}
